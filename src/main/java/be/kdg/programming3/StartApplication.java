package be.kdg.programming3;

import be.kdg.programming3.sms.presentation.View;
import be.kdg.programming3.sms.repository.HardcodedStudentRepository;
import be.kdg.programming3.sms.repository.StudentRepository;
import be.kdg.programming3.sms.service.StudentService;
import be.kdg.programming3.sms.service.StudentServiceImpl;

public class StartApplication {
    public static void main(String[] args) {
        //'glue' code
        StudentRepository studentRepository = new HardcodedStudentRepository();
        StudentService studentService = new StudentServiceImpl(studentRepository);
        View view = new View(studentService);
        view.showMenu();
    }
}
