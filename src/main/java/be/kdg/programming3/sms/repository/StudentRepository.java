package be.kdg.programming3.sms.repository;

import be.kdg.programming3.sms.domain.Student;

import java.util.List;

public interface StudentRepository {
    Student createStudent(Student student);

    List<Student> readStudents();
}
