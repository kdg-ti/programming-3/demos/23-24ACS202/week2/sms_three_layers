package be.kdg.programming3.sms.repository;

import be.kdg.programming3.sms.domain.Student;

import java.util.ArrayList;
import java.util.List;

public class HardcodedStudentRepository implements StudentRepository {
    private static List<Student> students = new ArrayList<>();

    @Override
    public Student createStudent(Student student){
        student.setId(students.size());
        students.add(student);
        return student;
    }

    @Override
    public List<Student> readStudents() {
        return students;
    }
}
