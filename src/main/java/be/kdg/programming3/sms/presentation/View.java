package be.kdg.programming3.sms.presentation;

import be.kdg.programming3.sms.service.StudentService;
import be.kdg.programming3.sms.service.StudentServiceImpl;

import java.time.LocalDate;
import java.util.Scanner;

public class View {
    private Scanner scanner = new Scanner(System.in);
    private StudentService studentService;

    public View(StudentService studentService){
        this.studentService = studentService;
    }

    public void showMenu() {
        while (true) {
            System.out.println("Make a choice:");
            System.out.println("1) show all students");
            System.out.println("2) add a student");
            System.out.print("Your choice:");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1 -> showAllStudents();
                case 2 -> addStudent();
            }
        }
    }

    private void addStudent() {
        scanner.nextLine();
        System.out.println("Adding a student:");
        System.out.print("\tName:");
        String name = scanner.nextLine();
        System.out.println("\tEnter the birthdate:");
        System.out.print("\t\tyear:");
        int year = scanner.nextInt();
        System.out.print("\t\tmonth:");
        int month = scanner.nextInt();
        System.out.print("\t\tday:");
        int day = scanner.nextInt();
        System.out.print("\tLength:");
        double length = scanner.nextDouble();
        studentService.addStudent(name, LocalDate.of(year, month, day), length);
    }

    private void showAllStudents() {
        studentService.getAllStudents().forEach(System.out::println);
    }
}
